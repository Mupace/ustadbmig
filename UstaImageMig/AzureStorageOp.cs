﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.DataMovement;
using MigrationLogEf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Usta.Repo;
using UstaMigStatic;
using UstaProdEf;

namespace UstaImageMig
{
    public class AzureStorageOp
    {
        #region global

        private CloudStorageAccount storageAcc { get; set; }

        private CloudBlobClient blobClient { get; set; }

        private CloudBlobContainer targetContainer { get; set; }
        
        private MigrationLogDbEntities logEntities { get; set; }

        private UstaProdDBEntities prodEntities { get; set; }
        
        private int currentRun { get; set; }

        private Repository log { get; set; }

        private Repository prodRepo { get; set; }
        
        #endregion

        public AzureStorageOp(int? currentRun)
        {
            logEntities = new MigrationLogDbEntities();
            prodEntities = new UstaProdDBEntities();

            log = new Repository(logEntities);
            prodRepo = new Repository(prodEntities);

            if(!currentRun.HasValue)
                currentRun = log.GetAll<MigLog>().Any() ? log.GetAll<MigLog>().Max(x => x.Run) + 1 : 1;

            storageAcc = CloudStorageAccount.Parse(ConfigurationSettings.AppSettings["StorageAccount"]);
            blobClient = storageAcc.CreateCloudBlobClient();
            try
            {
                targetContainer =
                    blobClient.GetContainerReference(ConfigurationSettings.AppSettings["TargetBlobContainer"]);
            }
            catch (Exception e)
            {
                LogData(LogLevels.CannotAccessStorage, "no id", "Cannot access to storage: "+e.Message);
            }
        }

        /// <summary>
        /// Planned to migrate from one blob container to another - skipped
        /// </summary>
        public void StartMigration()
        {
            
            //var list = GetListOfBlobItems();
            //MoveFile(list);
        }
       
        #region get blob names - commented out
        //This wasnt necessary to be written like this, I actually wanted to or not to
        //private List<string> GetListOfBlobItems()
        //{
        //    var blobList = sourceContainer.ListBlobs(null, false);
        //    List<string> blobNameList = new List<string>();

        //    foreach (var blob in blobList)
        //    {
        //        if (blob.GetType() == typeof(CloudBlockBlob))
        //        {
        //            CloudBlockBlob blockBlob = (CloudBlockBlob)blob;
        //            blobNameList.Add(blockBlob.Name);
        //        }
        //        else if (blob.GetType() == typeof(CloudBlobDirectory))
        //        {
        //            CloudBlobDirectory blobDir = (CloudBlobDirectory)blob;
        //            blobNameList.AddRange(GetItemsInBlobDir(blobDir));
        //        }
        //    }
        //    Console.WriteLine("Finished reading. {0} blobs found", blobNameList.Count);
        //    return blobNameList;
        //}

        //private List<string> GetItemsInBlobDir(CloudBlobDirectory dir)
        //{
        //    var blobList = dir.ListBlobs();
        //    List<string> blobNameList = new List<string>();
        //    foreach (var blob in blobList)
        //    {
        //        if (blob.GetType() == typeof(CloudBlockBlob))
        //        {
        //            CloudBlockBlob blockBlob = (CloudBlockBlob)blob;
        //            blobNameList.Add(blockBlob.Name);
        //        }
        //        else if (blob.GetType() == typeof(CloudBlobDirectory))
        //        {
        //            CloudBlobDirectory blobDir = (CloudBlobDirectory)blob;
        //            blobNameList.AddRange(GetItemsInBlobDir(blobDir));
        //        }
        //    }

        //    return blobNameList;
        //}
        
        #endregion


        public bool UploadFile(string fileSourceDir, string fullFileName)
        {
            string actualFileName = System.IO.Path.GetFileName(fullFileName);

            try
            {
                string blobName = fullFileName.Replace(fileSourceDir, string.Empty);
                CloudBlockBlob blobData = targetContainer.GetBlockBlobReference(blobName);
                blobData.UploadFromFile(fullFileName, FileMode.Open);

                Console.WriteLine("Uploaded: " + fullFileName);
            }
            catch (Exception e)
            {
                Console.WriteLine("Uploaded failed: " + fullFileName);
                LogData(LogLevels.CannotUploadToBlob, actualFileName, "Cannot upload: " + e.Message);
                return false;
            }

            return true;
        }

        private void GetBlockBlobAsStream(CloudBlobContainer container, string blobName, FileStream fs)
        {
            CloudBlockBlob blobData = container.GetBlockBlobReference(blobName);
            
            blobData.DownloadToStream(fs);
        }

        private void MoveFile(List<string> filenameList)
        {
            foreach(string filename in filenameList)
            {
                CloudBlob targetblob = targetContainer.GetBlobReference(filename);
                //string res = targetblob.StartCopy(
                //    new Uri(string.Format(@"{0}/{1}", sourceContainer.Uri, filename)));

            }

        }

        private void LogData(LogLevels level, string relatedItemId, string message)
        {
            MigLog toLog = logEntities.MigLogs.Create();

            toLog.Run = currentRun;
            toLog.LogType = level.ToString();
            toLog.RelatedTo = relatedItemId;
            toLog.LogMessage = message;
            toLog.LogDate = DateTime.Now;

            try
            {
                log.Insert<MigLog>(toLog);
                log.SaveChanges();
            }
            catch (Exception e)
            {
            }
        }
    }
}
