﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Usta.Repo;
using UstaMigStatic;
using System.Text.RegularExpressions;
using MigrationLogEf;
using UstaImageMig;

namespace UstaFilePro
{
    public class FileMover
    {
        #region global

        private string SourceDir { get; set; }

        private string TargetDir { get; set; }

        private Regex imageNamePrefix { get; set; }

        private readonly Regex ownerRegex = new Regex(@"(_)([0-9]+)(_)");

        private readonly Regex thumbRegex = new Regex(@"_t\.(jpg|JPG|jpeg|JPEG|gif|GIF|png|PNG)");

        private readonly Regex imageFileExtension = new Regex(@"\.(jpg|JPG|jpeg|JPEG|gif|GIF|png|PNG)");

        private MigrationLogDbEntities logEntities { get; set; }

        private int currentRun { get; set; }

        private Repository log { get; set; }

        private AzureStorageOp azureOp { get; set; }

        #endregion

        public FileMover(bool isCustomer)
        {
            if (isCustomer)
            {
                SourceDir = ConfigurationSettings.AppSettings["SourceFileDir_Customer"];
                TargetDir = ConfigurationSettings.AppSettings["TargetFileDir_Customer"];

                imageNamePrefix = new Regex(@"(User_)([0-9]+)(_)");
            }
            else
            {
                SourceDir = ConfigurationSettings.AppSettings["SourceFileDir"];
                TargetDir = ConfigurationSettings.AppSettings["TargetFileDir"];

                imageNamePrefix = new Regex(@"(Comp_)([0-9]+)(_)");
            }
            

            logEntities = new MigrationLogDbEntities();
            log = new Repository(logEntities);
            currentRun = log.GetAll<MigLog>().Any() ? log.GetAll<MigLog>().Max(x => x.Run) + 1 : 1;

            azureOp = new AzureStorageOp(currentRun);
        }

        public void StartMovingFiles()
        {
            List<string> fileList = new List<string>();
            try
            {
                fileList = GetFilesInDir(SourceDir);
                Console.WriteLine("Files read. Total: " + fileList.Count);

                
            }
            catch (Exception e)
            {
                LogData(LogLevels.CannotReadFiles, "no id", "Error on image file read: " + e.Message);
            }
            MoveFiles(fileList);
            Console.WriteLine("files moved to target dir");

            UploadFiles();
        }

        private List<string> GetFilesInDir(string dir) 
        {
            List<string> fileList = new List<string>();

            var itemList = Directory.GetFiles(dir);
            if (itemList.Any())
            {
                fileList.AddRange(itemList.ToList());
            }

            var directories = Directory.GetDirectories(dir);

            if (directories.Any())
            {
                foreach(var subDir in directories)
                {
                    var subItems = GetFilesInDir(subDir);
                    if (subItems.Any())
                    {
                        fileList.AddRange(subItems.ToList());
                    }
                }
            }

            return fileList;
        }

        private void MoveFiles(List<string> fileList)
        {
            foreach (var item in fileList)
            {
                var fileName = System.IO.Path.GetFileName(item);
                try
                {
                    var targetFileName = string.Empty;
                    var result = GenerateFilenameFromExistingName(fileName);
                    if (result.IsParsed)
                    {
                        targetFileName = result.Filename + "." + result.Extension;
                    }

                    if (string.IsNullOrEmpty(targetFileName))
                    {
                        continue;
                    }

                    string currentTargetDir = result.IsThumb ? string.Format("{0}{1}\\thumb\\", TargetDir, result.OwnerId) : string.Format("{0}{1}\\", TargetDir, result.OwnerId);

                    var dir = item.Replace(fileName, string.Empty);
                    dir = dir.Replace(SourceDir, currentTargetDir);

                    if (!Directory.Exists(dir))
                        Directory.CreateDirectory(dir);

                    var fullFileName = item.Replace(SourceDir, currentTargetDir);
                    fullFileName = fullFileName.Replace(fileName, targetFileName);

                    File.Copy(item, fullFileName, true);
                }
                catch (Exception e)
                {
                    LogData(LogLevels.FileCopyError, fileName, "Cannot move item:" + item);
                }
            }
        }

        /// <summary>
        /// Read from target folder and upload files
        /// </summary>
        private void UploadFiles()
        {
            Console.WriteLine("Starting upload");

            List<string> processedFiles = GetFilesInDir(TargetDir);

            Console.WriteLine(processedFiles.Count + " files found on target dir");
            Console.WriteLine("Starting upload files...");
            foreach (string fileName in processedFiles)
            {
                azureOp.UploadFile(TargetDir, fileName);
            }
        }

        private ImageResultModel GenerateFilenameFromExistingName(string filename)
        {
            ImageResultModel result = new ImageResultModel();
            if (string.IsNullOrEmpty(filename))
            {
                return result;
            }
            string rawFilename = filename.Trim();
            string hexWithExtension = imageNamePrefix.Replace(rawFilename, string.Empty);

            if (thumbRegex.IsMatch(hexWithExtension))
            {
                result.IsThumb = true;
                rawFilename = thumbRegex.Replace(hexWithExtension, string.Empty);
            }
            else
            {
                rawFilename = imageFileExtension.Replace(hexWithExtension, string.Empty);
            }
            
            if (string.IsNullOrEmpty(rawFilename))
            {
                return result;
            }

            var ownerResults = ownerRegex.Matches(filename);
            if (ownerResults.Count > 0)
            {
                string owner = ownerResults[0].ToString().Replace("_", string.Empty);
                long parsedOwner = 0;
                bool isOwnerParsed = long.TryParse(owner, System.Globalization.NumberStyles.Integer, null, out parsedOwner);

                if (isOwnerParsed)
                {
                    result.OwnerId = parsedOwner;
                }
                else
                {
                    return result;
                }
            }
            else
            {
                return result;
            }

            long parsed = 0;
            result.IsParsed = long.TryParse(rawFilename, System.Globalization.NumberStyles.HexNumber, null, out parsed);
            result.Filename = parsed;
            result.Extension = hexWithExtension.Replace(rawFilename, string.Empty).Replace(".", string.Empty).Replace("_t", string.Empty);

            return result;
        }

        private void LogData(LogLevels level, string relatedItemId, string message)
        {
            MigLog toLog = logEntities.MigLogs.Create();

            toLog.Run = currentRun;
            toLog.LogType = level.ToString();
            toLog.RelatedTo = relatedItemId;
            toLog.LogMessage = message;
            toLog.LogDate = DateTime.Now;

            try
            {
                log.Insert<MigLog>(toLog);
                log.SaveChanges();
            }
            catch (Exception e)
            {
            }
        }

    }
}
