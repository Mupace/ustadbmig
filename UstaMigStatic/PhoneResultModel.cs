﻿namespace UstaMigStatic
{
    public class PhoneResultModel
    {
        public bool LandlineParsed { get; set; }

        public string LandlineAreaCode { get; set; }

        public string LandlinePhoneNo { get; set; }

        public bool MobileParsed { get; set; }

        public string MobileCode { get; set; }

        public string MobileNo { get; set; }
    }
}