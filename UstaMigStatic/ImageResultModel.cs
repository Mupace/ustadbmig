﻿namespace UstaMigStatic
{
    public class ImageResultModel
    {
        public bool IsParsed { get; set; }

        public bool IsThumb { get; set; }

        public long OwnerId { get; set; }

        public long Filename { get; set; }

        public string Extension { get; set; }
    }
}