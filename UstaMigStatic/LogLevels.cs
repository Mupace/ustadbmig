﻿namespace UstaMigStatic
{
    public enum LogLevels
    {
        MissingData,
        InvalidPhoneValue,
        DbSave,
        SkipUsta,
        CannotAccessStorage,
        CannotFindTempFolder,
        CannotReadFiles,
        FileCopyError,
        CustomerFbIdParseError,
        CannotSaveCustomer,
        CannotUploadToBlob
    }
}