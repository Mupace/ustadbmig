﻿using System;

namespace UstaMigStatic
{
    public static class Randomizer
    {
        public static long GenerateLong()
        {
            const long min = 10000000000;

            const long max = 1000000000000;

            var rand = new Random();

            var buf = new byte[8];

            rand.NextBytes(buf);

            var longRand = BitConverter.ToInt64(buf, 0);

            var result = (Math.Abs(longRand % (max - min)) + min);

            return result;
        }
    }
}