//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UstaTargetEf
{
    using System;
    using System.Collections.Generic;
    
    public partial class UstaContact
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UstaContact()
        {
            this.Ustas = new HashSet<Usta>();
        }
    
        public long Id { get; set; }
        public long UstaId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string JobTitle { get; set; }
        public string MobileArea { get; set; }
        public string MobileNo { get; set; }
        public string Password { get; set; }
        public Nullable<long> FacebookId { get; set; }
        public Nullable<System.DateTime> LastLoginUtc { get; set; }
        public System.DateTime CreatedOnUtc { get; set; }
        public byte Status { get; set; }
        public string LandlineArea { get; set; }
        public string LandlineNo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Usta> Ustas { get; set; }
        public virtual Usta Usta { get; set; }
    }
}
