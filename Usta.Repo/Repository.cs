﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Usta.Repo
{
    public class Repository
    {
        private DbContext _dbContext { get; set; }

        public Repository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        #region IRepository<T> Members

        public void Insert<T>(T entity) where T : class
        {
            var dbSet = _dbContext.Set<T>();
            dbSet.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            var dbSet = _dbContext.Set<T>();
            dbSet.Remove(entity);
        }

        public IQueryable<T> SearchFor<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            var dbSet = _dbContext.Set<T>();
            return dbSet.Where(predicate);
        }

        public IQueryable<T> GetAll<T>() where T : class
        {
            var dbSet = _dbContext.Set<T>();
            return dbSet;
        }

        public T GetById<T>(int id) where T : class
        {
            var dbSet = _dbContext.Set<T>();
            return dbSet.Find(id);
        }

        public T GetById<T>(long id) where T : class
        {
            var dbSet = _dbContext.Set<T>();
            return dbSet.Find(id);
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        #endregion IRepository<T> Members
    }
}