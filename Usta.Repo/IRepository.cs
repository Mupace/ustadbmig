﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Usta.Repo
{
    public interface IRepository
    {
        void Insert<T>(T entity);

        void Delete<T>(T entity);

        IQueryable<T> SearchFor<T>(Expression<Func<T, bool>> predicate);

        IQueryable<T> GetAll<T>();

        T GetById<T>(int id);

        void SaveChanges();
    }
}