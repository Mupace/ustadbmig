﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Usta.Repo;
using UstaProdEf;

namespace UstaDbMig
{
    public class PhoneFilter
    {

        public void ProcessPhones()
        {
            List<Regex> regexList = new List<Regex>();

            regexList.Add(new Regex(@"(0[0-9])( )([0-9]{7})"));
            regexList.Add(new Regex(@"(0[0-9])([0-9]{7})"));
            regexList.Add(new Regex(@"(0[0-9]{9})"));
            regexList.Add(new Regex(@"\+[0-9]{3}\-[0-9]\-[0-9]{7}"));
            regexList.Add(new Regex(@"(\+)([0-9]{3})( )([0-9])( )([0-9]{4})( )([0-9]{3})"));
            regexList.Add(new Regex(@"([0-9]{3})( )([0-9]{3})( )([0-9]{4})"));
            regexList.Add(new Regex(@"([0-9]{3})( )([0-9]{3})([0-9]{4})"));
            regexList.Add(new Regex(@"(\+)( )([0-9]{3})( )([0-9])( )([0-9]{3})( )([0-9]{4})"));
            regexList.Add(new Regex(@"(\+)([0-9]{3})( )([0-9])( )([0-9]{7})"));
            regexList.Add(new Regex(@"(\+)([0-9]{12})"));
            regexList.Add(new Regex(@"(\+)([0-9]{3})( )([0-9]{2})( )([0-9]{4})( )([0-9]{3})"));
            regexList.Add(new Regex(@"(\+)([0-9]{11})"));
            regexList.Add(new Regex(@"(\+)([0-9]{3})(\-)([0-9]{2})(\-)([0-9]{7})"));
            regexList.Add(new Regex(@"(0)([0-9]{2})(\-)([0-9]{7})"));
            regexList.Add(new Regex(@"(\+)([0-9]{3})( )([0-9]{2})( )([0-9]{7})"));
            regexList.Add(new Regex(@"(\()(0)([0-9])(\))( )([0-9]{3})( )([0-9]{4})"));
            regexList.Add(new Regex(@"([0-9]{3})( )([0-9]{4})"));
            regexList.Add(new Regex(@"(0)([0-9]{2})( )([0-9]{3})( )([0-9]{2})( )([0-9]{2})"));
            regexList.Add(new Regex(@"(\+)([0-9]{3})( )([0-9]{2})( )([0-9]{3})( )([0-9]{2})( )([0-9]{2})"));
            regexList.Add(new Regex(@"(\+)([0-9]{3})( )([0-9]{2})( )([0-9]{3})( )([0-9]{4})"));
            regexList.Add(new Regex(@"(\+)([0-9]{3})( )([0-9]{1})( )([0-9]{3})( )([0-9]{4})"));
            regexList.Add(new Regex(@"(\+)([0-9]{3})(\-)([0-9]{8})"));
            regexList.Add(new Regex(@"([0-9]{2})( )([0-9]{2})( )([0-9]{2})( )([0-9]{2})( )([0-9]{1})"));
            regexList.Add(new Regex(@"(0)([0-9])( )([0-9]{3})( )([0-9]{4})"));
            regexList.Add(new Regex(@"(0)([0-9])(-)([0-9]{7})"));
            regexList.Add(new Regex(@"(0)([0-9]{2})(-)([0-9]{2})(-)([0-9]{3})(-)([0-9]{2})"));
            regexList.Add(new Regex(@"[0-9]{8}"));
            regexList.Add(new Regex(@"\+[0-9]{2}( )[0-9]{2}( )[0-9]{7}"));
            regexList.Add(new Regex(@"(0)[0-9]{2}( )[0-9]{2}( )[0-9]{2}( )[0-9]{3}"));
            regexList.Add(new Regex(@"\+( )[0-9]{12}"));
            regexList.Add(new Regex(@"[0-9]{3}( )[0-9]{2}( )[0-9]{3}"));
            regexList.Add(new Regex(@"\+[0-9]{3}( )[0-9]{3}( )[0-9]( )[0-9]{5}"));


            Repository prodRepo = new Repository(new UstaProdDBEntities());
            var companies = prodRepo.GetAll<UstaProdEf.Company>().Where(x => !string.IsNullOrEmpty(x.Telephone1) || !string.IsNullOrEmpty(x.Telephone2) || !string.IsNullOrEmpty(x.Fax)).ToList();
            var telephoneList = new List<string>();
            telephoneList.AddRange(companies.Where(x => !string.IsNullOrEmpty(x.Telephone1)).Select(x => x.Telephone1).ToList());
            telephoneList.AddRange(companies.Where(x => !string.IsNullOrEmpty(x.Telephone2)).Select(x => x.Telephone2).ToList());
            telephoneList.AddRange(companies.Where(x => !string.IsNullOrEmpty(x.Fax)).Select(x => x.Fax).ToList());

            StreamWriter sw = new StreamWriter(@"E:\results.txt");
            int counter = 0;
            foreach (var phone in telephoneList)
            {
                var isFound = false;
                foreach (var regex in regexList)
                {
                    if (regex.IsMatch(phone))
                    {
                        isFound = true;
                        break;
                    }
                }

                if (!isFound)
                {
                    Console.WriteLine(phone);
                    sw.WriteLine(phone);
                    counter++;
                }
            }

            sw.Close();
            Console.WriteLine("Total: " + counter.ToString());
        }
    }
}
