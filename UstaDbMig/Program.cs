﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Usta.Repo;
using UstaFilePro;
using UstaImageMig;
using UstaProdEf;

namespace UstaDbMig
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Press :");
                Console.WriteLine("1 for Usta db-images");
                Console.WriteLine("2 for Customer");
                Console.WriteLine("3 for Usta-File");
                Console.WriteLine("4 for Customer-File");
                Console.WriteLine("0 to exit");
                string response = Console.ReadLine();

                //string response = "3";

                if(response == "1")
                {
                    CompanyUserMigration migration = new CompanyUserMigration();
                    migration.StartMigrate();

                    Console.WriteLine("Op finished");
                    Console.ReadLine();
                }
                else if (response == "2")
                {
                    CompanyUserMigration migration = new CompanyUserMigration();
                    migration.StartCustomerMigration();

                    Console.WriteLine("Op finished");
                    Console.ReadLine();
                }
                else if(response == "3")
                {
                    Console.WriteLine("File op commencing...");
                    new FileMover(false).StartMovingFiles();

                    Console.WriteLine("Op finished");
                    Console.ReadLine();
                }
                else if (response == "4")
                {
                    Console.WriteLine("File op commencing...");
                    new FileMover(true).StartMovingFiles();

                    Console.WriteLine("Op finished");
                    Console.ReadLine();
                }
                else if (response == "0")
                {
                    //CompanyUserMigration mig = new CompanyUserMigration();
                    //mig.PhoneParse();

                    break;
                }
            }
             
            Console.WriteLine("BYE BYEEEEEE");
        }
    }
}
