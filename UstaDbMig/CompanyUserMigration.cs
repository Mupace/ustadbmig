﻿using System.Globalization;
using MigrationLogEf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Usta.Repo;
using UstaMigStatic;
using UstaProdEf;
using UstaTargetEf;

namespace UstaDbMig
{
    internal class CompanyUserMigration
    {
        #region global

        private readonly Regex imageNamePrefix = new Regex(@"(Comp_)([0-9]+)(_)");

        private readonly Regex customerImageNamePrefix = new Regex(@"(User_)([0-9]+)(_)");

        private readonly Regex imageFileExtension = new Regex(@"\.(jpg|JPG|jpeg|JPEG|gif|GIF|png|PNG)");

        private readonly Regex mobileArearRegex = new Regex(@"^(0?)(50|52|55|56)");

        private readonly Regex landlineAreaRegex = new Regex(@"^(0?)(2|3|4|6|7|9)");

        private readonly Regex notNumber = new Regex(@"\D");

        private readonly Regex countryCode = new Regex(@"^(\+?|(00)?)971");

        private MigrationLogDbEntities logEntities { get; set; }

        private UstaProdDBEntities prodEntities { get; set; }

        private UstaTargetEntities targetEntities { get; set; }

        private int currentRun { get; set; }

        private Repository log { get; set; }

        private Repository prodRepo { get; set; }

        private Repository targetRepo { get; set; }

        private const string nameFormat = "{0} {1} {2}";

        #endregion global

        public CompanyUserMigration()
        {
            logEntities = new MigrationLogDbEntities();
            prodEntities = new UstaProdDBEntities();
            targetEntities = new UstaTargetEntities();

            log = new Repository(logEntities);
            prodRepo = new Repository(prodEntities);
            targetRepo = new Repository(targetEntities);

            currentRun = log.GetAll<MigLog>().Any() ? log.GetAll<MigLog>().Max(x => x.Run) + 1 : 1;
        }
        
        #region phoneParse
        
        public void PhoneParse()
        {
            var rawData = prodRepo.GetAll<Company>();
            List<string> phoneNumberList = new List<string>();

            phoneNumberList.AddRange(rawData.Where(x => !string.IsNullOrEmpty(x.Telephone1)).Select(x => x.Telephone1).ToList());
            phoneNumberList.AddRange(rawData.Where(x => !string.IsNullOrEmpty(x.Telephone2)).Select(x => x.Telephone2).ToList());
            phoneNumberList.AddRange(rawData.Where(x => !string.IsNullOrEmpty(x.Fax)).Select(x => x.Fax).ToList());

            ProcessPhoneNumber(phoneNumberList);
        }

        private PhoneResultModel ProcessPhoneNumber(List<string> phoneNumberList)
        {
            PhoneResultModel model = new PhoneResultModel();
            foreach (var phoneNumber in phoneNumberList)
            {
                if (string.IsNullOrEmpty(phoneNumber))
                    continue;

                string rawNumber = notNumber.Replace(phoneNumber, string.Empty); //clear out empty space, signs
                rawNumber = countryCode.Replace(rawNumber, string.Empty); // clear out country code

                if (string.IsNullOrEmpty(rawNumber))
                    continue;

                if (mobileArearRegex.IsMatch(rawNumber))
                {
                    model.MobileParsed = true;
                    model.MobileNo = mobileArearRegex.Replace(rawNumber, string.Empty);
                    model.MobileCode = rawNumber.Replace(model.MobileNo, string.Empty);

                    if (model.MobileNo.Length > 7)
                        model.MobileParsed = false;
                }
                else if (landlineAreaRegex.IsMatch(rawNumber))
                {
                    model.LandlineParsed = true;
                    model.LandlinePhoneNo = landlineAreaRegex.Replace(rawNumber, string.Empty);
                    model.LandlineAreaCode = rawNumber.Replace(model.LandlinePhoneNo, string.Empty);

                    if (model.LandlinePhoneNo.Length > 7)
                        model.LandlineParsed = false;
                }
                else
                {
                    LogData(LogLevels.InvalidPhoneValue, rawNumber, "Raw phone number logged, cannot be parsed");
                }
            }

            return model;
        }
        
        #endregion

        #region ImageName parse

        /// <summary>
        /// Test action for image name convert of usta images
        /// </summary>
        public void ConvertImageNames()
        {
            List<string> fileNameList = prodRepo.GetAll<CompanyImage>().ToList().Select(x => x.ImageFileName).ToList();

            foreach (var fileName in fileNameList)
            {
                var result = GenerateFilenameFromExistingName(fileName, imageNamePrefix);
                if (result.IsParsed)
                {
                    int x = 0;
                }
            }

            Console.WriteLine("End of Operation. Processed: " + fileNameList.Count);
        }

        private ImageResultModel GenerateFilenameFromExistingName(string filename, Regex prefixRegex)
        {
            ImageResultModel result = new ImageResultModel();
            if (string.IsNullOrEmpty(filename))
            {
                return result;
            }
            string rawFilename = filename.Trim();
            string hexWithExtension = prefixRegex.Replace(rawFilename, string.Empty);
            rawFilename = imageFileExtension.Replace(hexWithExtension, string.Empty);

            if (string.IsNullOrEmpty(rawFilename))
            {
                return result;
            }
            
            long parsed = 0;
            result.IsParsed = long.TryParse(rawFilename, System.Globalization.NumberStyles.HexNumber, null, out parsed);
            result.Filename = parsed;
            result.Extension = hexWithExtension.Replace(rawFilename, string.Empty).Replace(".", string.Empty);

            return result;
        }

        #endregion ImageName parse

        #region Usta-Contact-Category migration

        public void StartMigrate()
        {
            IQueryable<UstaProdEf.Company> allCompanies = prodRepo.GetAll<UstaProdEf.Company>();
            List<UstaTargetEf.CategorySub> categories = targetRepo.GetAll<UstaTargetEf.CategorySub>().ToList();
            
            foreach (var company in allCompanies)
            {
                ProcessCompany(company, categories);
            }
        }

        private void ProcessCompany(UstaProdEf.Company company, List<UstaTargetEf.CategorySub> categories)
        {
            try
            {
                var existingUsta = targetRepo.GetById<UstaTargetEf.Usta>(company.Id);
                if (existingUsta != null)
                {
                    DeleteUsta(existingUsta);
                    Console.WriteLine("Deleted: " + company.Id);
                }
            }
            catch (Exception e)
            {
                
            }

            UstaTargetEf.Usta usta = targetEntities.Ustas.Create();

            usta.Id = company.Id;
            usta.Name = company.CompanyName;
            usta.Status = company.IsActive ? (byte)2 : (byte)3;
            usta.CreatedOnUtc = company.CreatedOnUtc.HasValue ? company.CreatedOnUtc.Value : company.CreatedOn;
            usta.Website = company.Website;
            usta.About = company.Description;
            usta.UstaType = company.UstaType;
            usta.Latitude = company.Latitude;
            usta.Longitude = company.Longitude;
            usta.Address = company.Address;
            usta.BranchName = company.BranchName;
            usta.Is247Available = company.Is724.HasValue ? company.Is724.Value : false;
            
            var phoneParseResult = ProcessPhoneNumber(new List<string>() {company.Telephone1, company.Telephone2});

            if (phoneParseResult.MobileParsed || phoneParseResult.LandlineParsed)
            {
                if (phoneParseResult.MobileParsed)
                {
                    usta.MobileArea = phoneParseResult.MobileCode;
                    usta.MobileNo = phoneParseResult.MobileNo;
                }
                if (phoneParseResult.LandlineParsed)
                {
                    usta.LandlineArea = phoneParseResult.LandlineAreaCode;
                    usta.LandlineNo = phoneParseResult.LandlinePhoneNo;
                }
            }
            else
            {
                Console.WriteLine(usta.Id.ToString() + " - No valid phone number, skipping usta");
                LogData(LogLevels.SkipUsta, usta.Id.ToString(), "No valid phone number, skipping usta");
                return;
            }

            #region contact
            
            List<CompanyContact> contactList = company.CompanyContacts.ToList();
            if (contactList.Any(x => !string.IsNullOrEmpty(x.Email) && !string.IsNullOrEmpty(x.Password)))
            {
                usta.UstaContacts = new Collection<UstaContact>();

                foreach (var comContact in contactList)
                {
                    if (string.IsNullOrEmpty(comContact.Email) || string.IsNullOrEmpty(comContact.Password))
                        continue;

                    UstaContact cnt = targetEntities.UstaContacts.Create();

                    cnt.Id = comContact.Id;
                    cnt.UstaId = company.Id;
                    cnt.Name =
                        string.Format(nameFormat, comContact.FirstName, comContact.MiddleName, comContact.LastName)
                            .Trim();
                    cnt.Email = comContact.Email;
                    cnt.Password = comContact.Password;
                    cnt.LastLoginUtc = comContact.LastLogin;

                    //These 2 here new to db; they havent been there ---
                    cnt.MobileArea = usta.MobileArea;
                    cnt.MobileNo = usta.MobileNo;

                    if (comContact.CreatedOnUtc.HasValue)
                        cnt.CreatedOnUtc = comContact.CreatedOnUtc.Value;

                    cnt.Status = comContact.IsActive ? (byte)2 : (byte)3;
                    usta.UstaContacts.Add(cnt);
                }
            }
            
            #endregion

            //If no contact dont save it
            if (!usta.UstaContacts.Any())
            {
                Console.WriteLine(usta.Id + " - Has no valid contact info");
                LogData(LogLevels.SkipUsta, usta.Id.ToString(), "Has no valid contact info");
                return;
            }

            #region category
            
            List<CompanyCategory> categoryList = company.CompanyCategories.ToList();

            if (categoryList.Any())
            {
                usta.UstaCategories = new Collection<UstaCategory>();

                foreach (var cat in categoryList)
                {
                    UstaCategory category = targetEntities.UstaCategories.Create();
                    var targetCategory = categories.FirstOrDefault(x => x.DisplayId == cat.CategoryId);
                    if (targetCategory != null)
                    {
                        category.Id = cat.Id;
                        category.UstaId = cat.CompanyId;
                        category.CategoryId = targetCategory.Id;

                        usta.UstaCategories.Add(category);
                    }
                }
            }

            #endregion


            #region pictures

            List<CompanyImage> imageList = company.CompanyImages.ToList();

            if (imageList.Any())
            {
                usta.UstaImages = new Collection<UstaImage>();

                foreach (var image in imageList)
                {
                    var parseResult = GenerateFilenameFromExistingName(image.ImageFileName, imageNamePrefix);
                    if (!parseResult.IsParsed)
                    {
                        continue;
                    }

                    UstaImage imageToAdd = targetEntities.UstaImages.Create();

                    imageToAdd.Id = parseResult.Filename;
                    imageToAdd.UstaId = usta.Id;
                    imageToAdd.FileExtension = parseResult.Extension;
                    imageToAdd.CreatedOnUtc = image.CreatedOnUtc.HasValue ? image.CreatedOnUtc.Value : image.CreatedOn;
                    imageToAdd.Status = (byte)3;
                    imageToAdd.VerifiedOnUtc = null;
                    imageToAdd.IsProfileImage = image.IsProfilePicture;

                    usta.UstaImages.Add(imageToAdd);
                }    
            }

            #endregion

            try
            {
                Console.WriteLine(usta.Id);
                targetRepo.Insert<UstaTargetEf.Usta>(usta);
                targetRepo.SaveChanges();

                bool hasExtraReferences = false;
                //If none, add this id as primary -- done here due to circular reference
                if (!usta.PrimaryContactId.HasValue)
                {
                    usta.PrimaryContactId = usta.UstaContacts.First().Id;
                    hasExtraReferences = true;
                }

                if (!usta.ProfileImageId.HasValue)
                {
                    var proImage = usta.UstaImages.FirstOrDefault(x => x.IsProfileImage);
                    if (proImage == null)
                    {
                        proImage = usta.UstaImages.FirstOrDefault();
                    }

                    if (proImage != null)
                    {
                        usta.ProfileImageId = proImage.Id;
                        usta.ProfileImageExtension = proImage.FileExtension;
                        hasExtraReferences = true;
                    }
                }

                if (hasExtraReferences)
                {
                    targetRepo.SaveChanges();
                }
            }
            catch (Exception e)
            {
                LogData(LogLevels.DbSave, usta.Id.ToString(), "Data cannot be saved to database.");
                Console.WriteLine("Errorr");
            }
        }

        private void DeleteUsta(UstaTargetEf.Usta usta)
        {
            if (usta.UstaImages.Any())
            {
                var images = usta.UstaImages.ToList();
                foreach (var ustaImage in images)
                {
                    targetRepo.Delete(ustaImage);
                }
                targetRepo.SaveChanges();
            }

            if (usta.UstaCategories.Any())
            {
                var categories = usta.UstaCategories.ToList();
                foreach (var ustaCat in categories)
                {
                    targetRepo.Delete(ustaCat);
                }
                targetRepo.SaveChanges();
            }

            if (usta.UstaContacts.Any())
            {
                usta.PrimaryContactId = null;
                targetRepo.SaveChanges();
                var contacts = usta.UstaContacts.ToList();
                foreach (var contact in contacts)
                {
                    targetRepo.Delete(contact);
                }
                targetRepo.SaveChanges();
            }

            targetRepo.Delete(usta);
            targetRepo.SaveChanges();
        }


        #endregion Usta-Contact-Category migration

        #region customer migration

        public void StartCustomerMigration()
        {
            Console.WriteLine("Op started");
            IQueryable<UstaProdEf.User> userList = prodRepo.GetAll<UstaProdEf.User>();

            foreach (var user in userList)
            {
                ProcessUser(user);
            }
        }

        private void ProcessUser(UstaProdEf.User user)
        {
            try
            {
                var existingCustomer = targetRepo.GetById<UstaTargetEf.Customer>(user.Id);
                if (existingCustomer != null)
                {
                    DeleteCustomer(existingCustomer);
                    Console.WriteLine("Deleted: " + user.Id);
                }
            }
            catch (Exception e)
            {

            }

            Customer customer = targetEntities.Customers.Create();

            customer.Id = user.Id;
            customer.Name = user.FullName;
            customer.Email = user.Email;
            customer.Password = user.Password;
            if (!string.IsNullOrEmpty(user.FBProfile))
            {
                try
                {
                    customer.FacebookId = long.Parse(user.FBProfile);
                }
                catch (Exception e)
                {
                    LogData(LogLevels.CustomerFbIdParseError, user.Id.ToString(), "Just log-op continues; Cannot parse fb profile: " + user.FBProfile);
                }
            }
            customer.CreatedOnUtc = user.CreatedOnUtc.HasValue ? user.CreatedOnUtc.Value : user.CreatedOn;
            customer.Status = user.IsActive ? (byte)1 : (byte)0;

            #region Customer Image

            List<UserImage> userImageList = user.UserImages.ToList();

            if (userImageList.Any())
            {
                customer.CustomerImages = new Collection<CustomerImage>();

                foreach (var image in userImageList)
                {
                    ImageResultModel result = GenerateFilenameFromExistingName(image.ImageFileName,
                        customerImageNamePrefix);

                    if (result.IsParsed)
                    {
                        CustomerImage imageToAdd = targetEntities.CustomerImages.Create();
                        imageToAdd.Id = result.Filename;
                        imageToAdd.CustomerId = user.Id;
                        imageToAdd.FileExtension = result.Extension;
                        imageToAdd.CreatedOnUtc = image.CreatedOnUtc.HasValue
                            ? image.CreatedOnUtc.Value
                            : image.CreatedOn;
                        imageToAdd.Status = image.IsActive ? (byte) 1 : (byte) 0;

                        customer.CustomerImages.Add(imageToAdd);
                    }
                }
            }
            
            #endregion

            if (customer.CustomerImages.Any())
            {
                customer.CustomerImages.First().IsProfileImage = true;
            }

            try
            {
                targetRepo.Insert(customer);
                targetRepo.SaveChanges();

                if (customer.CustomerImages.Any())
                {
                    var image = customer.CustomerImages.First();

                    customer.ProfileImageId = image.Id;
                    customer.ProfileImageExtension = image.FileExtension;

                    targetRepo.SaveChanges();
                }
                Console.WriteLine(customer.Id);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error customer: "+ user.Id);
                LogData(LogLevels.CannotSaveCustomer, user.Id.ToString(), "failed on save customer: " + e.Message);
            }
        }

        private void DeleteCustomer(UstaTargetEf.Customer customer)
        {
            if (customer.CustomerImages.Any())
            {
                var images = customer.CustomerImages.ToList();
                foreach (var image in images)
                {
                    targetRepo.Delete(image);
                }

                targetRepo.SaveChanges();
            }

            targetRepo.Delete(customer);
            targetRepo.SaveChanges();
        }

        #endregion

        private void LogData(LogLevels level, string relatedItemId, string message)
        {
            MigLog toLog = logEntities.MigLogs.Create();

            toLog.Run = currentRun;
            toLog.LogType = level.ToString();
            toLog.RelatedTo = relatedItemId;
            toLog.LogMessage = message;
            toLog.LogDate = DateTime.Now;

            try
            {
                log.Insert<MigLog>(toLog);
                log.SaveChanges();
            }
            catch (Exception e)
            {
            }
        }
    }
}