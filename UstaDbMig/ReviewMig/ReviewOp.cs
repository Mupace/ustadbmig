﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MigrationLogEf;
using Usta.Repo;
using UstaProdEf;
using UstaTargetEf;

namespace UstaDbMig.ReviewMig
{
    public class ReviewOp
    {
        private MigrationLogDbEntities logEntities { get; set; }

        private UstaProdDBEntities prodEntities { get; set; }

        private UstaTargetEntities targetEntities { get; set; }

        private int currentRun { get; set; }

        private Repository log { get; set; }

        private Repository prodRepo { get; set; }

        private Repository targetRepo { get; set; }

        private List<long> UstaIdList { get; set; }

        private List<long> CustomerIdList { get; set; }

        public ReviewOp()
        {
            logEntities = new MigrationLogDbEntities();
            prodEntities = new UstaProdDBEntities();
            targetEntities = new UstaTargetEntities();

            log = new Repository(logEntities);
            prodRepo = new Repository(prodEntities);
            targetRepo = new Repository(targetEntities);

            currentRun = log.GetAll<MigLog>().Any() ? log.GetAll<MigLog>().Max(x => x.Run) + 1 : 1;
        }


        public void StartMigratio()
        {
            var reviews = prodRepo.GetAll<UstaProdEf.Review>().Where(x => x.ParentReviewId.HasValue == false).ToList();
            UstaIdList = targetRepo.GetAll<UstaTargetEf.Usta>().Select(x => x.Id).ToList();
            CustomerIdList = targetRepo.GetAll<UstaTargetEf.Customer>().Select(x => x.Id).ToList();

            if (UstaIdList.Count == 0 || CustomerIdList.Count == 0)
            {
                Console.WriteLine("No user or customer on target db");
                return;
            }

            foreach (var review in reviews)
            {
                ProcessReview(review);
            }
        }


        private void ProcessReview(UstaProdEf.Review review)
        {
            

            //if(review.xxxCompanyId.HasValue && review.xxxIsCreatedByUser.HasValue.


        }
    }
}
