//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UstaProdEf
{
    using System;
    
    public partial class p_JobImagesGetByJobId_Result
    {
        public long Id { get; set; }
        public long JobId { get; set; }
        public short ImageIndex { get; set; }
        public string ImageFileName { get; set; }
        public bool IsApproved { get; set; }
        public bool IsActive { get; set; }
        public bool IsProfilePicture { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<System.DateTime> CreatedOnUtc { get; set; }
    }
}
