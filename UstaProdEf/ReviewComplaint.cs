//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UstaProdEf
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReviewComplaint
    {
        public long Id { get; set; }
        public long ReviewId { get; set; }
        public long UserId { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<System.DateTime> CreatedOnUtc { get; set; }
    
        public virtual Review Review { get; set; }
        public virtual User User { get; set; }
    }
}
