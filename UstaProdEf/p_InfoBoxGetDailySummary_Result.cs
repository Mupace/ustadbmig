//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UstaProdEf
{
    using System;
    
    public partial class p_InfoBoxGetDailySummary_Result
    {
        public Nullable<int> UstaCount { get; set; }
        public Nullable<int> RegisteredUstaCount { get; set; }
        public Nullable<int> NewUstaCount { get; set; }
        public Nullable<int> JobCount { get; set; }
        public Nullable<int> ReviewCount { get; set; }
    }
}
